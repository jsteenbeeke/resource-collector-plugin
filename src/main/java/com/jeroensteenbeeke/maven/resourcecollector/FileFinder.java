package com.jeroensteenbeeke.maven.resourcecollector;

import java.io.File;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.Nonnull;

public class FileFinder {

	
	private final File directory;

	private final String extension;

	public FileFinder(@Nonnull File directory, @Nonnull String extension) {
		if (!directory.isDirectory()) {
			throw new IllegalArgumentException(String.format("%s is not a directory", directory.getAbsolutePath()));
		}
		this.directory = directory;
		this.extension = extension;
	}

	public Map<String, File> getPathsToFile() {
		return scanDir("", directory);
	}

	private Map<String, File> scanDir(@Nonnull String prefix, @Nonnull File base) {
		Map<String, File> result = new TreeMap<>();
		
		final String newPrefixBase = prefix.isEmpty() ? "" : prefix.concat(Const.FILE_SEPARATOR);

		Arrays.stream(base.listFiles(File::isDirectory)).forEach(
				f -> result.putAll(scanDir(newPrefixBase.concat(f.getName()), f)));
		Arrays.stream(base.listFiles(f -> f.getName().endsWith(extension))).filter(File::exists).forEach(
				f -> result.put(newPrefixBase.concat(f.getName()), f));

		return result;
	}

}
