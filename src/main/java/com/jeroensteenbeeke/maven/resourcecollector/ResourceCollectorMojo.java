package com.jeroensteenbeeke.maven.resourcecollector;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.Objects;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.DefaultArtifact;
import org.apache.maven.artifact.handler.ArtifactHandler;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.model.Dependency;
import org.apache.maven.model.Plugin;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.util.FileUtils;
import org.sonatype.plexus.build.incremental.BuildContext;

import com.google.common.io.Files;

import static java.util.stream.Collectors.joining;

@Mojo(defaultPhase = LifecyclePhase.GENERATE_SOURCES, name = "collect", threadSafe = true, requiresDependencyResolution = ResolutionScope.TEST)
public class ResourceCollectorMojo extends AbstractMojo {
	@Parameter(required = true,
			   defaultValue = "${project.build.directory}/generated-sources/collected-resources")
	public File outputDirectory;

	@Parameter(required=true)
	public File[] directories;

	@Parameter(required = true)
	public String[] extensions;

	@Component
	public BuildContext buildContext;

	@Parameter(required = true, property = "project")
	protected MavenProject project;

	@Parameter(readonly = true, required = true, defaultValue = "${localRepository}")
	protected ArtifactRepository localRepository;

	@Component
	private ArtifactHandler artifactHandler;

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		getLog().info("Scanning for resource files of type "+ Arrays.stream(extensions)
																	.map(e -> e.startsWith(".") ?
																			e : ".".concat(e))
				.collect
				(joining(", ", "[", "]")));

		if (!outputDirectory.exists()) {
			outputDirectory.mkdirs();
		}

		project.addCompileSourceRoot(outputDirectory.getPath());

		for (File resourceDirectory: directories) {
			for (String extension: extensions) {
				new FileFinder(resourceDirectory, extension).getPathsToFile().forEach(this::copy);
			}
		}


		if (!buildContext.isIncremental()) {
			getLog().info("Checking dependencies for resources");

			project.getBuildPlugins()
				   .stream()
				   .filter(plugin -> {
						boolean proceed = Const.GROUP_ID.equals(plugin
								.getGroupId())
								&& Const.ARTIFACT_ID.equals(plugin
										.getArtifactId());

						getLog().debug(
								"Are we " + plugin.getGroupId() + ":"
										+ plugin.getArtifactId() + "? "
										+ proceed);

						return proceed;
					}) //
				   .map(Plugin::getDependencies) //
				   .flatMap(List::stream)
				   //
				   .map((Dependency dep) -> localRepository.find(new DefaultArtifact(dep
						   .getGroupId(), dep.getArtifactId(), dep
						   .getVersion(), dep.getScope(), dep.getType(),
						   dep.getClassifier(), artifactHandler))) //
				   .filter(Objects::nonNull) //
				   .map(this::artifactToJar) //
				   .filter(Objects::nonNull) //
				   .forEach(this::processJar);
		} else {
			getLog().warn("Incremental build, not checking dependencies");
		}

		if (buildContext != null) {
			buildContext.refresh(outputDirectory);
		}
	}

	public JarFile artifactToJar(Artifact artifact) {
		File file = artifact.getFile();

		if (!file.exists()) {
			file = new File(file.getAbsolutePath().concat(
					".".concat(artifact.getType())));
		}

		if (isJar(file)) {
			try {
				getLog().info("Including jar file " + file.getName());
				return new JarFile(file);
			} catch (IOException e) {
				throw new UncheckedIOException(e);
			}
		}

		return null;

	}

	public void processJar(JarFile jar) {
		int extracted = 0;

		File targetDirectory = Files.createTempDir();
		
		// Step 1, handle all directories

		Enumeration<JarEntry> entries = jar.entries();
		while (entries.hasMoreElements()) {
			JarEntry entry = entries.nextElement();
			File target = new File(targetDirectory, entry.getName());

			if (entry.isDirectory()) {
				target.mkdirs();
			}
		}
		
		entries = jar.entries();
		while (entries.hasMoreElements()) {
			JarEntry entry = entries.nextElement();
			if (entry.isDirectory()) {
				continue;
			}
			
			File target = new File(targetDirectory, entry.getName());
			
			try (InputStream is = jar.getInputStream(entry);
					FileOutputStream out = new FileOutputStream(target)) {
				int n;

				while ((n = is.read()) != -1) {
					out.write(n);
				}
			} catch (IOException e) {
				throw new UncheckedIOException(e);
			}

			extracted++;

		}

		getLog().info("Extracted " + extracted + " files from " + jar.getName());

		for (String extension: extensions) {
			new FileFinder(targetDirectory, extension).getPathsToFile()
															  .forEach(this::copy);
		}
	}

	public void copy(String path, File file) {
		File target = determineTargetFile(path);

		try {
			if (FileUtils.contentEquals(file, target)) {
				getLog().info("\tFile not changed: " + file.getName());
				return;
			}

			Files.copy(file, target);
			getLog().info(
					"\tCopied " + file.getName() + " to "
							+ target.getParentFile().getAbsolutePath());
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	private File determineTargetFile(String path) {
		String[] pathParts = path.split(sanitizeFileSeparator(Const.FILE_SEPARATOR));

		File base = outputDirectory;

		final int lastIndex = pathParts.length - 1;

		for (int i = 0; i < lastIndex; i++) {
			base = new File(base, pathParts[i]);
			if (!base.exists() && !base.mkdirs()) {
				throw new RuntimeException(new MojoFailureException(
						"Could not create output directory "
								+ base.getAbsolutePath()));
			} else if (!base.isDirectory()) {
				throw new RuntimeException(new MojoFailureException(
						"Required output directory " + base.getAbsolutePath()
								+ " is not a directory"));
			}
		}

		File target = new File(base, pathParts[lastIndex]);
		return target;
	}

	private String sanitizeFileSeparator(String fileSeparator) {
		if ("\\".equals(fileSeparator)) {
			return "\\\\";
		}

		return fileSeparator;
	}

	public boolean isJar(File file) {
		boolean isJar = file.getName().toLowerCase()
				.endsWith(Const.EXTENSION_JAR);

		getLog().info("Dependency " + file.getName() + " is jar? " + isJar);

		return isJar;
	}

}
