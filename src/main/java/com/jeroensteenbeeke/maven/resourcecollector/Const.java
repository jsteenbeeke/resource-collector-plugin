package com.jeroensteenbeeke.maven.resourcecollector;

public class Const {
	public static final String FILE_SEPARATOR = System.getProperty("file.separator");
	
    public static final String GROUP_ID = "com.jeroensteenbeeke.maven";

    public static final String ARTIFACT_ID = "resource-collector-maven-plugin";

    public static final String EXTENSION_JAR = ".jar";


}
